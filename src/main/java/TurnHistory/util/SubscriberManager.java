package TurnHistory.util;

import TurnHistory.TurnHistory;
import TurnHistory.hooks.PreCombatRoomForegroundRenderSubscriber;
import TurnHistory.hooks.PreMonsterActionSubscriber;
import basemod.interfaces.ISubscriber;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.ArrayList;

public class SubscriberManager {
	private static ArrayList<ISubscriber> toRemove;
	private static ArrayList<PreMonsterActionSubscriber> preMonsterActionSubscribers;
	private static ArrayList<PreCombatRoomForegroundRenderSubscriber> preCombatRoomForegroundRenderSubscribers;

	public static void initialize() {
		toRemove = new ArrayList<>();
		preMonsterActionSubscribers = new ArrayList<>();
		preCombatRoomForegroundRenderSubscribers = new ArrayList<>();
	}

	public static void publishPreMonsterAction(AbstractMonster monster) {
		TurnHistory.logger.info("publishPreMonsterAction");

		for (PreMonsterActionSubscriber sub : preMonsterActionSubscribers) {
			sub.receivePreMonsterAction(monster);
		}
		unsubscribeLaterHelper(PreMonsterActionSubscriber.class);
	}

	public static void publishPreCombatRoomForegroundRender(SpriteBatch sb) {
		// We do *not* log on render

		for (PreCombatRoomForegroundRenderSubscriber sub : preCombatRoomForegroundRenderSubscribers) {
			sub.receivePreCombatRoomForegroundRender(sb);
		}
		unsubscribeLaterHelper(PreCombatRoomForegroundRenderSubscriber.class);
	}

	private static void unsubscribeLaterHelper(Class<? extends ISubscriber> removalClass) {
		for (ISubscriber sub : toRemove)  {
			if  (removalClass.isInstance(sub)) {
				unsubscribe(sub, removalClass);
			}
		}
	}

	private static <T> void subscribeIfInstance(ArrayList<T> list, ISubscriber sub, Class<T> clazz) {
		if (clazz.isInstance(sub)) {
			list.add(clazz.cast(sub));
		}
	}

	private static <T> void unsubscribeIfInstance(ArrayList<T> list, ISubscriber sub, Class<T> clazz) {
		if (clazz.isInstance(sub)) {
			list.remove(clazz.cast(sub));
		}
	}

	public static void subscribe(ISubscriber sub) {
		subscribeIfInstance(preMonsterActionSubscribers, sub, PreMonsterActionSubscriber.class);
		subscribeIfInstance(preCombatRoomForegroundRenderSubscribers, sub, PreCombatRoomForegroundRenderSubscriber.class);
	}

	public static void subscribe(ISubscriber sub, Class<? extends ISubscriber> additionClass) {
		if (additionClass.isInstance(PreMonsterActionSubscriber.class)) {
			preMonsterActionSubscribers.add((PreMonsterActionSubscriber) sub);
		} else if (additionClass.isInstance(PreCombatRoomForegroundRenderSubscriber.class)) {
			preCombatRoomForegroundRenderSubscribers.add((PreCombatRoomForegroundRenderSubscriber) sub);
		}
	}

	public static void unsubscribe(ISubscriber sub) {
		unsubscribeIfInstance(preMonsterActionSubscribers, sub, PreMonsterActionSubscriber.class);
	}

	public static void unsubscribe(ISubscriber sub, Class<? extends ISubscriber> removalClass) {
		if (removalClass.equals(PreMonsterActionSubscriber.class)) {
			preMonsterActionSubscribers.remove((PreMonsterActionSubscriber) sub);
		} else if (removalClass.isInstance(PreCombatRoomForegroundRenderSubscriber.class)) {
			preCombatRoomForegroundRenderSubscribers.remove((PreCombatRoomForegroundRenderSubscriber) sub);
		}
	}

	public static void unsubscribeLater(ISubscriber sub) {
		toRemove.add(sub);
	}

	static {
		initialize();
	}
}
