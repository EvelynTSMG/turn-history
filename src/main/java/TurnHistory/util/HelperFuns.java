package TurnHistory.util;

import com.badlogic.gdx.math.Vector2;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.helpers.Hitbox;

public class HelperFuns {
	public static boolean isBadCard(AbstractCard card) {
		return card.type == AbstractCard.CardType.STATUS || card.type == AbstractCard.CardType.CURSE;
	}

	public static boolean pointIsInHitbox(Vector2 point, Hitbox hitbox) {
		return pointIsInHitbox(point.x, point.y, hitbox);
	}

	public static boolean pointIsInHitbox(float x, float y, Hitbox hitbox) {
		float hitboxL = hitbox.cX - hitbox.width/2;
		float hitboxR = hitbox.cX + hitbox.width/2;

		float hitboxD = hitbox.cY - hitbox.height/2;
		float hitboxU = hitbox.cY + hitbox.height/2;

		return x > hitboxL && x < hitboxR && y > hitboxD && y < hitboxU;
	}
}
