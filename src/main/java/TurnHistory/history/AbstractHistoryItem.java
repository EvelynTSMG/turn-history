package TurnHistory.history;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.megacrit.cardcrawl.helpers.Hitbox;

public abstract class AbstractHistoryItem {
	public Vector2 pos = new Vector2();
	public Vector2 targetPos = new Vector2();
	private Vector2 startPos = new Vector2();
	public Interpolation posInterp = Interpolation.linear;
	public float interpTimer = 0f;
	public float interpTime = 0.5f;
	public Hitbox hitbox;
	public float alpha;
	public float alphaTarget;
	public boolean addable = true;

	public void setPos(float x, float y) {
		pos.x = x;
		pos.y = y;
		hitbox.move(x, y);
	}

	public void setTargetPos(float x, float y) {
		// only update startPos if targetPos changes
		if (targetPos.x != x && targetPos.y != y) {
			TurnHistory.TurnHistory.logger.info(String.format("Changed target pos to (%f, %f)", x, y));
			startPos = new Vector2(pos);
			interpTimer = 0f;
		}

		targetPos.x = x;
		targetPos.y = y;
	}

	public void move() {
		// move intent to target pos
		if (pos.dst(targetPos) >= 0.5f) {
			interpTimer += Gdx.graphics.getDeltaTime();
			if (interpTimer > interpTime) {
				interpTimer = interpTime;
			}

			float progress = posInterp.apply(interpTimer/interpTime);
			pos.x = MathUtils.lerp(startPos.x, targetPos.x, progress);
			pos.y = MathUtils.lerp(startPos.y, targetPos.y, progress);
		} else {
			pos.set(targetPos);
		}

		hitbox.move(pos.x, pos.y);
		hitbox.update();
	}

	public abstract float getHeight();
	public abstract float getWidth();
	public abstract float getMarginVert();
	public abstract float getMarginWhore();
	public abstract void update();
	public abstract void render(SpriteBatch sb);
}
