package TurnHistory.history;

import TurnHistory.TurnHistory;
import TurnHistory.helper.AbstractPanel;
import TurnHistory.helper.Intent;
import TurnHistory.util.HelperFuns;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.ArrayList;
import java.util.Iterator;

public class HistoryPanel extends AbstractPanel {
	public static float WIDTH;
	public static float HEIGHT;
	public final ArrayList<AbstractHistoryItem> historyItems = new ArrayList<>();
	public final ArrayList<AbstractHistoryItem> toAdd = new ArrayList<>();
	public Hitbox hitbox;
	public int startIdx = 0;

	public HistoryPanel() {
		super(TurnHistory.HISTORY_X + WIDTH/2f, TurnHistory.HISTORY_TOP - HEIGHT/2f,
				-TurnHistory.HISTORY_X - WIDTH/2f, TurnHistory.HISTORY_TOP - HEIGHT/2f,
				null, false);

		hitbox = new Hitbox(TurnHistory.HISTORY_X, TurnHistory.HISTORY_TOP, WIDTH, HEIGHT);
	}

	public void addIntent(AbstractMonster monster, ArrayList<AbstractGameAction> actions) {
		// Copy intent from actions
		Intent newIntent = new Intent(monster, actions, true);
		newIntent.setPos(-TurnHistory.HISTORY_X - WIDTH * 2, TurnHistory.HISTORY_TOP);

		toAdd.add(0, newIntent);

		// point the intents to their new positions
		pointItems();
	}

	public void clear() {
		historyItems.clear();
	}

	public void clampStartIdx() {
		if (startIdx < 0) {
			startIdx = 0;
		}

		if (startIdx >= historyItems.size())  {
			startIdx = historyItems.size() - 1;
		}
	}

	public void pointItems() {
		for (int i = 0; i < historyItems.size(); i++) {
			AbstractHistoryItem item = historyItems.get(i);

			float targetX = pos.x;
			float targetY = TurnHistory.HISTORY_TOP - item.getHeight()/2f;

			if (i < historyItems.size() - 1) {
				AbstractHistoryItem prevItem = historyItems.get(i + 1);
				targetY = prevItem.pos.y - prevItem.getHeight()/2f - prevItem.getMarginVert();
			}

			if (targetY < hitbox.cY - HEIGHT/2) {
				targetX = hidePos.x - item.getWidth();
				targetY = TurnHistory.HISTORY_BOTTOM;
			} else if (targetY > hitbox.cY + HEIGHT/2 || i < startIdx) {
				targetX = hidePos.x - item.getWidth();
				targetY = TurnHistory.HISTORY_TOP;
			}

			if(isHidden) {
				item.setTargetPos(-hidePos.x - item.getWidth(), targetY);
			} else {
				item.setTargetPos(targetX, targetY);
			}

			if (HelperFuns.pointIsInHitbox(item.targetPos, hitbox)) {
				item.hitbox.resize(item.getWidth(), item.getHeight());
			} else {
				item.hitbox.resize(0f, 0f);
			}
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		Intent.updateGlobals();

		hitbox.move(pos.x, pos.y);
		hitbox.update();
		hitbox.render(sb);

		if (AbstractDungeon.isScreenUp || AbstractDungeon.getCurrRoom().isBattleOver) {
			hide();
		} else {
			show();
		}
		updatePositions();

		// >:(
		Iterator<AbstractHistoryItem> i = toAdd.iterator();
		while (i.hasNext()) {
			AbstractHistoryItem item = i.next();
			item.update();

			if (item.addable) {
				historyItems.add(item);
				i.remove();
			}
		}

		pointItems();
		for (AbstractHistoryItem item : historyItems) {
			item.render(sb);
		}
	}

	static {
		WIDTH = Settings.WIDTH * 0.1f * Settings.scale;
		HEIGHT = TurnHistory.HISTORY_TOP - TurnHistory.HISTORY_BOTTOM;
	}
}
