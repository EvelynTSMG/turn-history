package TurnHistory.fixes;

import com.evacipated.cardcrawl.modthespire.lib.SpirePatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePostfixPatch;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class MakeTempCardTargetFix {
	@SpirePatch2(clz = MakeTempCardAtBottomOfDeckAction.class, method = SpirePatch.CONSTRUCTOR)
	@SpirePatch2(clz = MakeTempCardInDiscardAction.class, method = SpirePatch.CONSTRUCTOR,
			paramtypez = { AbstractCard.class, int.class })
	@SpirePatch2(clz = MakeTempCardInDiscardAndDeckAction.class, method = SpirePatch.CONSTRUCTOR)
	@SpirePatch2(clz = MakeTempCardInDrawPileAction.class, method = SpirePatch.CONSTRUCTOR,
			paramtypez = { AbstractCard.class, int.class,
					boolean.class, boolean.class, boolean.class,
					float.class, float.class })
	@SpirePatch2(clz = MakeTempCardInHandAction.class, method = SpirePatch.CONSTRUCTOR,
			paramtypez = { AbstractCard.class, int.class })
	@SpirePatch2(clz = MakeTempCardInHandAction.class, method = SpirePatch.CONSTRUCTOR, // idfk
			paramtypez = { AbstractCard.class, boolean.class })
	public static class PostConstructor {
		@SpirePostfixPatch
		public static void giveTarget(AbstractGameAction __instance) {
			__instance.target = AbstractDungeon.player;
		}
	}
}
