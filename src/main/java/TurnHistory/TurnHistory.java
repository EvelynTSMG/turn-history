package TurnHistory;

import TurnHistory.history.HistoryPanel;
import TurnHistory.hooks.PreCombatRoomForegroundRenderSubscriber;
import TurnHistory.hooks.PreMonsterActionSubscriber;

import TurnHistory.util.SubscriberManager;
import basemod.BaseMod;
import basemod.interfaces.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.GameActionManager;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.input.InputHelper;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.potions.AbstractPotion;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

@SpireInitializer
public class TurnHistory implements
		// Adder?
		PostInitializeSubscriber,
		// Misc
		OnPlayerTurnStartPostDrawSubscriber,
		OnCardUseSubscriber,
		PreMonsterTurnSubscriber,
		PreMonsterActionSubscriber,
		PostPotionUseSubscriber,
		PostBattleSubscriber,
		PostDeathSubscriber,
		// Update & Rendering
		PostUpdateSubscriber,
		PreCombatRoomForegroundRenderSubscriber {
	public static final Logger logger = LogManager.getLogger(TurnHistory.class.getName());
	private static final String modId = "TurnHistory";

	// Mod-settings settings
	// None :3

	// In-game mod settings panel
	private static final String MODNAME = "Turn History";
	private static final String AUTHOR = "EvelynTSMG";
	private static final String DESCRIPTION = "Adds a turn history display in combat!";

	public static float TOOTH = 2f;

	public static final float LERP_SPEED = 7f;
	public static float HISTORY_X;
	public static float HISTORY_TOP;
	public static float HISTORY_SPLIT_SIZE;
	public static float HISTORY_BOTTOM;
	public static HistoryPanel historyPanel;

	// really stupid
	private static AbstractGameAction firstAction;
	private static AbstractGameAction lastAction;

	public TurnHistory() {
		BaseMod.subscribe(this);
		SubscriberManager.subscribe(this);

	}

	public static void initialize() {
		logger.info("========================= Initializing Turn History Mod =========================");
		TurnHistory turnHistory = new TurnHistory();
		logger.info("========================| Turn History Mod Initialized |=========================");
	}

	@Override
	public void receivePostInitialize() {
		HISTORY_X = 0f;
		HISTORY_TOP = Settings.HEIGHT * 0.825f; // top 17.5%
		HISTORY_SPLIT_SIZE = 96f * Settings.scale; // new entry every 96px * scale
		HISTORY_BOTTOM = Settings.HEIGHT * 0.175f; // bottom 17.5%

		historyPanel = new HistoryPanel();
	}

	@Override
	public void receiveOnPlayerTurnStartPostDraw() {
		logger.log(Level.INFO, String.format("Turn: %d", GameActionManager.turn));
	}

	@Override
	public void receiveCardUsed(AbstractCard card) {
		logger.log(Level.INFO, String.format("Used card: %s", card.name));
	}

	@Override
	public boolean receivePreMonsterTurn(AbstractMonster monster) {
		firstAction = AbstractDungeon.actionManager.actions.get(0);
		lastAction = AbstractDungeon.actionManager.actions.get(AbstractDungeon.actionManager.actions.size() - 1);
		return true;
	}

	@Override
	public void receivePreMonsterAction(AbstractMonster monster) {
		// Check what actions where added by monster
		ArrayList<AbstractGameAction> frontActions = new ArrayList<>();
		ArrayList<AbstractGameAction> backActions = new ArrayList<>();

		ArrayList<AbstractGameAction> actions = AbstractDungeon.actionManager.actions;

		for (AbstractGameAction action : actions) {
			if (action == firstAction) {
				break;
			}
			frontActions.add(action);
		}

		for (int i = actions.size() - 1; i >= 0; i--) {
			if (actions.get(i) == lastAction) {
				break;
			}
			backActions.add(0, actions.get(i));
		}

		// Combine to send to new intent
		frontActions.addAll(backActions);

		historyPanel.addIntent(monster, frontActions);
	}

	@Override
	public void receivePostPotionUse(AbstractPotion potion) {
		logger.log(Level.INFO, String.format("Used potion: %s", potion.name));
	}

	@Override
	public void receivePostBattle(AbstractRoom room) {
		historyPanel.clear();
	}

	@Override
	public void receivePostDeath() {
		historyPanel.clear();
	}

	@Override
	public void receivePostUpdate() {
		if (historyPanel.hitbox.hovered) {
			if (InputHelper.scrolledUp) {
				historyPanel.startIdx -= 1;
			} else if (InputHelper.scrolledDown) {
				historyPanel.startIdx += 1;
			}
			historyPanel.clampStartIdx();
		}
	}

	@Override
	public void receivePreCombatRoomForegroundRender(SpriteBatch spriteBatch) {
		historyPanel.render(spriteBatch);
	}
}