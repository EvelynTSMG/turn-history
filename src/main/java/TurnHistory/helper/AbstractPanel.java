package TurnHistory.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.megacrit.cardcrawl.core.Settings;

public class AbstractPanel {
	public static final float SNAP_THRESHOLD = 0.5f;
	public final Vector2 hidePos = new Vector2();
	public final Vector2 showPos = new Vector2();
	public final Vector2 targetPos = new Vector2();
	public final Vector2 pos = new Vector2();
	public Texture img;
	protected float img_width;
	protected float img_height;
	public boolean isHidden;
	public boolean doneAnimating;

	public AbstractPanel(float show_x, float show_y, float hide_x, float hide_y, Texture image, boolean startHidden) {
		isHidden = false;
		doneAnimating = true;
		img = image;
		showPos.x = show_x;
		showPos.y = show_y;
		hidePos.x = hide_x;
		hidePos.y = hide_y;
		if (img != null) {
			img_width = (float)img.getWidth() * Settings.scale;
			img_height = (float)img.getHeight() * Settings.scale;
		}

		if (startHidden) {
			pos.x = hide_x;
			pos.y = hide_y;
			targetPos.x = hide_x;
			targetPos.y = hide_y;
			isHidden = true;
		} else {
			pos.x = show_x;
			pos.y = show_y;
			targetPos.x = show_x;
			targetPos.y = show_y;
			isHidden = false;
		}

	}

	// I don't do the if checks here cause too lazy to optimize
	public void show() {
		targetPos.set(showPos);
		isHidden = false;
		doneAnimating = false;
	}

	public void hide() {
		targetPos.set(hidePos);
		isHidden = true;
		doneAnimating = false;
	}

	public void updatePositions() {
		if (!pos.equals(targetPos)) {
			pos.lerp(targetPos, Gdx.graphics.getDeltaTime() * TurnHistory.TurnHistory.LERP_SPEED);
			if (pos.dst(targetPos) < SNAP_THRESHOLD) {
				pos.set(targetPos);
				doneAnimating = true;
			} else {
				doneAnimating = false;
			}
		}
	}

	public void render(SpriteBatch sb) {
		if (img != null) {
			sb.setColor(Color.WHITE);
			sb.draw(img, pos.x, pos.y, img_width, img_height, 0, 0, img.getWidth(), img.getHeight(), false, false);
		}
	}
}
