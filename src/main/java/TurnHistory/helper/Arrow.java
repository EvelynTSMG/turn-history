package TurnHistory.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.ImageMaster;

import java.util.ArrayList;
import java.util.Objects;

public class Arrow {
	public final float WIDTH = 256f;
	public final float HEIGHT = 256f;
	public final float DOT_WIDTH = 128f;
	public final float DOT_HEIGHT = 128f;
	public Vector2 startVector = new Vector2();
	public Vector2 controlPoint = new Vector2();
	public Vector2 endVector = new Vector2();
	public ArrayList<Color> colors = new ArrayList<>();
	public float scale = 1f;
	public float scaleTimer = 0f;
	public Vector2[] points = new Vector2[20];
	private Vector2 tmp = new Vector2();

	public Arrow() {}

	public Arrow(float x1, float y1, float x2, float y2) {
		this(x1, y1, MathUtils.lerp(x1, x2, 0.3f),  y2 * 1.5f, x2, y2, null);
	}

	public Arrow(float x1, float y1, float x2, float y2, float x3, float y3) {
		this(x1, y1, x2, y2, x3, y3, null);
	}

	public Arrow(float x1, float y1, float x2, float y2, float x3, float y3, ArrayList<Color> colors) {
		if (colors == null) {
			this.colors.add(Color.WHITE.cpy());
		} else {
			this.colors = colors;
		}

		startVector.set(x1, y1);
		controlPoint.set(x2, y2);
		endVector.set(x3, y3);

		// Determine actual points[] length
		float distPerPoint = Settings.WIDTH / 60f;
		int pointCount = (int)(Math.abs(x3 - x1) / distPerPoint);
		points = new Vector2[pointCount];

		for(int i = 0; i < points.length; i++) {
			points[i] = new Vector2();
		}
	}

	public void render(SpriteBatch sb) {
		Vector2 ctrlPoint = new Vector2();
		if (controlPoint == null) {
			ctrlPoint.x = startVector.x - (endVector.x - startVector.x) / 4f;
			ctrlPoint.y = startVector.y + (endVector.y - startVector.y) / 2f;
		} else {
			ctrlPoint.x = controlPoint.x;
			ctrlPoint.y = controlPoint.y;
		}

		if (ctrlPoint.y > startVector.y + points.length * (Settings.HEIGHT/80f)) {
			ctrlPoint.y = startVector.y + points.length * (Settings.HEIGHT/80f);
		}


		scaleTimer += Gdx.graphics.getDeltaTime();
		if (scaleTimer > 1f) {
			scaleTimer = 1f;
		}

		float targetScale = Settings.scale * 1.2f;
		if (Math.abs(controlPoint.x - startVector.x) < Settings.WIDTH/80f) {
			targetScale = Settings.scale;
		}

		scale = Interpolation.elasticOut.apply(Settings.scale, targetScale, scaleTimer);

		tmp.x = ctrlPoint.x - endVector.x;
		tmp.y = ctrlPoint.y - endVector.y;
		tmp.nor();

		drawCurvedLine(sb, startVector, endVector, controlPoint);

		// idk if this is right but it works
		int colorIdx = (points.length + 1) % colors.size();
		sb.setColor(colors.get(colorIdx));

		float w = WIDTH;
		float h = HEIGHT;
		sb.draw(ImageMaster.TARGET_UI_ARROW, endVector.x - w/2f, endVector.y - h/2f,
				w/2f, h/2f, w, h,
				scale, scale, tmp.angle() + 90f,
				0, 0, (int)w, (int)h,
				false, false);
	}

	public void drawCurvedLine(SpriteBatch sb, Vector2 start, Vector2 end, Vector2 control) {
		float radius = 7f * Settings.scale;

		float w = DOT_WIDTH;
		float h = DOT_HEIGHT;

		for(int i = 0; i < points.length; i++) {
			points[i] = Bezier.quadratic(points[i], (float)i / (float)points.length, start, control, end, tmp);
			radius += 0.4F * Settings.scale;

			sb.setColor(colors.get(i % colors.size()));
			if (i != 0) {
				tmp.x = points[i - 1].x - points[i].x;
				tmp.y = points[i - 1].y - points[i].y;
				sb.draw(ImageMaster.TARGET_UI_CIRCLE, points[i].x - w/2f, points[i].y - h/2f,
						w/2f, h/2f, w, h,
						radius / 18f, radius / 18f, tmp.nor().angle() + 90f,
						0, 0, (int)w, (int)h,
						false, false);
			} else {
				tmp.x = control.x - points[i].x;
				tmp.y = control.y - points[i].y;
				sb.draw(ImageMaster.TARGET_UI_CIRCLE, points[i].x - w/2f, points[i].y - h/2f,
						w/2f, h/2f, w, h,
						radius / 18f, radius / 18f, tmp.nor().angle() + 270f,
						0, 0, (int)w, (int)h,
						false, false);
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Arrow arrow = (Arrow) o;
		return startVector.equals(arrow.startVector) && Objects.equals(controlPoint, arrow.controlPoint) && endVector.equals(arrow.endVector);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startVector, controlPoint, endVector);
	}
}
