package TurnHistory.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.Hitbox;

public class Reticle {
	public static final Texture CORNER = new Texture(Gdx.files.internal("images/ui/combat/reticleCorner.png"),
			Pixmap.Format.LuminanceAlpha, false);
	public static final float OFFSET_DIST = 15f * Settings.scale;

	public Hitbox target;
	public Color color = new Color(1f, 1f, 1f, 0f);
	public Color shadowColor = new Color(0f, 0f, 0f, 0f);
	public boolean isRendered = false;
	public float alpha = 0f;
	public float offset = 0f;
	public float animTimer = 0f;

	public Reticle() {}

	public Reticle(Hitbox target, Color color) {
		this.target = target;
		this.color = color;
	}

	public void update() {
		// I have no idea how this works
		if (isRendered) {
			isRendered = false;
			alpha += Gdx.graphics.getDeltaTime() * 3f;
			if (alpha > 1f) {
				alpha = 1f;
			}

			animTimer += Gdx.graphics.getDeltaTime();
			if (animTimer > 1f) {
				animTimer = 1f;
			}

			offset = Interpolation.elasticOut.apply(OFFSET_DIST, 0f, animTimer);
		} else {
			alpha = 0f;
			animTimer = 0f;
			offset = OFFSET_DIST;
		}
	}

	public void render(SpriteBatch sb) {
		isRendered = true;
		update();
		renderReticleCorner(sb, -target.width / 2f + offset, target.height / 2f - offset, false, false);
		renderReticleCorner(sb, target.width / 2f - offset, target.height / 2f - offset, true, false);
		renderReticleCorner(sb, -target.width / 2f + offset, -target.height / 2f + offset, false, true);
		renderReticleCorner(sb, target.width / 2f - offset, -target.height / 2f + offset, true, true);
	}

	private void renderReticleCorner(SpriteBatch sb, float x, float y, boolean flipX, boolean flipY) {
		shadowColor.a = alpha / 4f;
		sb.setColor(shadowColor);
		sb.draw(CORNER,
				target.cX + x - 18f + 4f * Settings.scale, target.cY + y - 18f - 4f * Settings.scale,
				18f, 18f, 36f, 36f,
				Settings.scale, Settings.scale, 0f,
				0, 0, 36, 36,
				flipX, flipY);

		color.a = alpha;
		sb.setColor(color);
		sb.draw(CORNER,
				target.cX + x - 18f, target.cY + y - 18f,
				18f, 18f, 36f, 36f,
				Settings.scale, Settings.scale, 0f,
				0, 0, 36, 36,
				flipX, flipY);
	}

	static {
		CORNER.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
	}
}
