package TurnHistory.helper;

import TurnHistory.TurnHistory;
import TurnHistory.hacks.AbstractMonsterHacks;
import TurnHistory.history.AbstractHistoryItem;
import TurnHistory.util.HelperFuns;
import basemod.ReflectionHacks;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.esotericsoftware.spine.Skeleton;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.*;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.PowerTip;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.vfx.AbstractGameEffect;
import com.megacrit.cardcrawl.vfx.BobEffect;
import com.megacrit.cardcrawl.vfx.DebuffParticleEffect;
import com.megacrit.cardcrawl.vfx.ShieldParticleEffect;
import com.megacrit.cardcrawl.vfx.combat.BuffParticleEffect;
import com.megacrit.cardcrawl.vfx.combat.StunStarEffect;
import com.megacrit.cardcrawl.vfx.combat.UnknownParticleEffect;

import java.util.ArrayList;

public class Intent extends AbstractHistoryItem {
	private static final Color ATTACK_COLOR = new Color(1f, 0.2f, 0.3f, 1f);
	private static final Color BUFF_COLOR = new Color(0.65f, 1f, 1f, 1f);
	private static final Color DEBUFF_COLOR =  new Color(0.65f, 0.85f, 0.5f, 1f);
	private static final Color STUN_COLOR = new Color(1f, 0.9f,  0.5f, 1f);

	// For some reason leaving it as default isn't the same
	public static BobEffect globalBobEffect = new BobEffect(2f);
	public static float globalAngle = 0f;

	public BobEffect bobEffect = globalBobEffect;
	public ArrayList<AbstractGameEffect> vfx = new ArrayList<>();
	public AbstractMonster.Intent type;
	public PowerTip tip;
	public float offsetX;
	public float angle = globalAngle;
	public float particleTimer = 0f;
	public Texture img;
	public Texture bg;
	public int dmg;
	public int baseDmg;
	public int multiAmt;
	public Color color;
	public AbstractCreature monster;
	public ArrayList<AbstractGameAction> actions;
	public boolean actionsFinalized = false;
	public ArrayList<Arrow> arrows = new ArrayList<>();
	boolean targetsSelf;
	public Reticle reticle = new Reticle();

	public Intent(AbstractMonster monster, ArrayList<AbstractGameAction> actions) {
		this(monster, actions, true);
	}

	public Intent(AbstractMonster monster, ArrayList<AbstractGameAction> actions, boolean useGlobalValues) {
		if (!useGlobalValues) {
			bobEffect = AbstractMonsterHacks.getBobEffect(monster);
			angle = AbstractMonsterHacks.getIntentAngle(monster);
		}

		hitbox = new Hitbox(pos.x, pos.y, monster.intentHb.width, monster.intentHb.height);
		type = monster.intent;
		tip = AbstractMonsterHacks.getIntentTip(monster);
		alpha = monster.intentAlpha;
		alphaTarget = monster.intentAlphaTarget;
		offsetX = monster.intentOffsetX;

		// Probably the only thing that's fine to not deep copy
		img = AbstractMonsterHacks.getIntentImg(monster);
		bg = AbstractMonsterHacks.getIntentBg(monster);

		dmg = monster.getIntentDmg();
		baseDmg = monster.getIntentBaseDmg();
		multiAmt = AbstractMonsterHacks.getIntentMultiAmt(monster);
		color = AbstractMonsterHacks.getIntentColor(monster);

		reticle.target = monster.hb;

		this.monster = monster;
		this.actions = actions;

		addable = false;
		loadTargets(); // check now
	}

	public static void updateGlobals() {
		globalBobEffect.update();
		globalAngle += Gdx.graphics.getDeltaTime() * 150f;
	}

	public static Color getColorBasedOnIntent(Intent intent) {
		// TODO: Make different colors depending on actions, e.g. DEFEND_DEBUFF is green @ player and blue @ monster
		switch (intent.type) {
			case ATTACK: case ATTACK_BUFF: case ATTACK_DEBUFF: case ATTACK_DEFEND:
				return ATTACK_COLOR.cpy();
			case BUFF: case DEFEND: case DEFEND_BUFF:
				return BUFF_COLOR.cpy();
			case DEBUFF: case STRONG_DEBUFF: case DEFEND_DEBUFF:
				return DEBUFF_COLOR.cpy();
			case SLEEP: case STUN:
				return STUN_COLOR.cpy();
			case MAGIC:
				return Color.PURPLE.cpy();
			default:
				return Color.WHITE.cpy();
		}
	}

	public static Color getColorBasedOnAction(AbstractGameAction action) {
		switch (action.actionType) {
			case DAMAGE:
				return new Color(1f, 0.2f, 0.3f, 1f);
			case BLOCK: case HEAL:
				return new Color(0.65f, 1f, 1f, 1f);
			case POWER: {
				AbstractPower powerToApply = ReflectionHacks.getPrivate(action, ApplyPowerAction.class, "powerToApply");
				if (powerToApply.type == AbstractPower.PowerType.BUFF) {
					return BUFF_COLOR.cpy();
				} else if (powerToApply.type == AbstractPower.PowerType.DEBUFF) {
					return DEBUFF_COLOR.cpy();
				} else { // future-proofing / mod-proofing
					return STUN_COLOR.cpy();
				}
			}
			case DEBUFF:
				return DEBUFF_COLOR.cpy();
			case WAIT:
				return STUN_COLOR.cpy();
			case CARD_MANIPULATION: {
				TurnHistory.logger.info(String.format("Trying to find out if %s is making a bad card", action.getClass().getName()));
				if (action instanceof MakeTempCardInDiscardAction) {
					if (HelperFuns.isBadCard(getCardFromAction(MakeTempCardInDiscardAction.class,
							action, "c"))) {
						return DEBUFF_COLOR.cpy();
					}
				} else if (action instanceof MakeTempCardInHandAction) {
					if (HelperFuns.isBadCard(getCardFromAction(MakeTempCardInHandAction.class,
							action, "c"))) {
						return DEBUFF_COLOR.cpy();
					}
				} else if (action instanceof MakeTempCardInDiscardAndDeckAction) {
					if (HelperFuns.isBadCard(getCardFromAction(MakeTempCardInDiscardAndDeckAction.class,
							action, "cardToMake"))) {
						return DEBUFF_COLOR.cpy();
					}
				} else if (action instanceof MakeTempCardInDrawPileAction) {
					if (HelperFuns.isBadCard(getCardFromAction(MakeTempCardInDrawPileAction.class,
							action, "cardToMake"))) {
						return DEBUFF_COLOR.cpy();
					}
				}
			}
			default:
				return Color.WHITE.cpy();
		}
	}

	private static <T extends AbstractGameAction> AbstractCard getCardFromAction(Class<T> tClass, AbstractGameAction action, String fieldName) {
		try {
			return ReflectionHacks.getPrivate(action, tClass, fieldName);
		} catch (Exception ex) {
			return null;
		}
	}

	public void loadTargets() {
		if (!actionsFinalized && actions.stream().allMatch(a -> a.isDone)) {
			for (AbstractGameAction action : actions) {
				AbstractCreature source = action.source;
				AbstractCreature target = action.target;

				if (source == null) {
					source = monster;
				}

				if (target != null) {
					Color actionColor = getColorBasedOnAction(action);
					// Fall back to intent-based if color not found in action-based
					if (actionColor.equals(Color.WHITE)) {
						TurnHistory.logger.info(
								String.format("Color not found for action %s, choosing color for intent %s instead.",
										action.getClass().getName(), type.name()));
						actionColor = getColorBasedOnIntent(this);
					}

					if (target == source) {
						targetsSelf = true;
						reticle.color = actionColor;
					} else {
						int flipX = 1;

						if (target.hb.cX > source.hb.cX) {
							flipX = -1;
						}

						// Control point figured out automagically in Arrow constructor
						Arrow newArrow = new Arrow(source.hb.cX - (source.hb.width / 2f) * flipX,
								source.hb.cY + source.hb.height * 3f / 8f, // 0.75 halved
								action.target.hb.cX + (action.target.hb.width / 2f) * flipX,
								action.target.hb.cY + action.target.hb.height * 3f / 8f); // 0.75 halved


						if (arrows.contains(newArrow)) {
							Arrow oldArrow = arrows.get(arrows.indexOf(newArrow));
							if (!oldArrow.colors.contains(actionColor)) {
								oldArrow.colors.add(actionColor);
							}
						} else {
							arrows.add(newArrow);
							arrows.get(arrows.size() - 1).colors.set(0, actionColor);
						}
					}
				}
			}

			actionsFinalized = true;
			addable = true;
		}
	}

	public void updateIntentVfx() {
		if (alpha > 0.0F) {
			particleTimer -= Gdx.graphics.getDeltaTime();

			if (particleTimer < 0f) {
				switch (type) {
					case ATTACK_DEBUFF:
					case DEBUFF:
					case STRONG_DEBUFF:
					case DEFEND_DEBUFF: {
						particleTimer = 1f;
						vfx.add(new DebuffParticleEffect(pos.x, pos.y));
						break;
					}

					case ATTACK_BUFF:
					case BUFF:
					case DEFEND_BUFF: {
						particleTimer = 0.1f;
						vfx.add(new BuffParticleEffect(pos.x, pos.y));
						break;
					}

					case ATTACK_DEFEND: {
						particleTimer = 0.5f;
						vfx.add(new ShieldParticleEffect(pos.x, pos.y));
						break;
					}

					case UNKNOWN: {
						particleTimer = 0.5f;
						vfx.add(new UnknownParticleEffect(pos.x, pos.y));
						break;
					}

					case STUN: {
						particleTimer= 0.67f;
						vfx.add(new StunStarEffect(pos.x, pos.y));
						break;
					}
				}
			}

			for (AbstractGameEffect e : vfx) {
				e.update();
			}
		}
	}

	public void update() {
		// shhhh
		loadTargets();
	}

	public void render(SpriteBatch sb) {
		if (actionsFinalized) {
			move();
			hitbox.render(sb);
			if (hitbox.hovered) {
				if (monster.isDead) {
					renderTransMonster(sb);
				}

				for (Arrow arrow : arrows) {
					arrow.render(sb);
				}

				if (targetsSelf) {
					reticle.render(sb);
				}
			} else {
				for (Arrow arrow : arrows) {
					arrow.scaleTimer = 0f;
				}
				reticle.update();
			}

			// render intent last so arrow is underneath
			renderIntent(sb);
		}
	}

	public void renderIntent(SpriteBatch sb) {
		renderIntent(sb, false, true, true);
	}

	public void renderIntent(SpriteBatch sb, boolean doBob, boolean doRotation, boolean doVfx) {
		updateIntentVfx();
		bobEffect.update();

		if (doVfx) {
			renderIntentVfx(sb, true);
		}

		renderIntentMain(sb, doBob, doRotation);

		if (doVfx) {
			renderIntentVfx(sb, false);
		}

		renderIntentDamage(sb, doBob);
	}

	private  void renderIntentDamage(SpriteBatch sb, boolean animate) {
		if (type.name().contains("ATTACK")) {
			float bobOffset = 0f;
			if (animate) {
				bobOffset = bobEffect.y;
			}

			if (multiAmt > 0) {
				FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont,
						dmg + "x" + multiAmt,
						pos.x - 30f * Settings.scale,
						pos.y + bobOffset - 12f * Settings.scale,
						color);
			} else {
				FontHelper.renderFontLeftTopAligned(sb, FontHelper.topPanelInfoFont,
						String.valueOf(dmg),
						pos.x - 30f * Settings.scale,
						pos.y + bobOffset - 12f * Settings.scale,
						color);
			}
		}
	}

	private void renderIntentVfx(SpriteBatch sb, boolean renderBehind) {
		for (AbstractGameEffect e : vfx) {
			if (e.renderBehind == renderBehind) {
				e.render(sb);
			}
		}
	}

	private void renderIntentMain(SpriteBatch sb, boolean doBob, boolean doRotation) {
		color.a = alpha;
		sb.setColor(color);

		float scale = Settings.scale;

		if (Settings.isMobile) {
			scale *= 1.2f;
		}

		float bobOffset = 0f;
		if (doBob) {
			bobOffset = bobEffect.y;
		}

		if (bg != null) {
			float bgW = bg.getWidth();
			float bgH = bg.getHeight();
			sb.setColor(new Color(1f, 1f, 1f, alpha / 2f));
			sb.draw(bg, pos.x - bgW/2, pos.y - bgH/2 + bobOffset,
					bgW/2, bgH/2, bgW, bgH, scale, scale,
					0f, 0, 0, (int)bgW, (int)bgH,
					false, false);
		}

		if (img != null
				&& type != AbstractMonster.Intent.UNKNOWN
				&& type != AbstractMonster.Intent.STUN) {
			if (doRotation
					&& (type == AbstractMonster.Intent.DEBUFF
					|| type == AbstractMonster.Intent.STRONG_DEBUFF)) {
				angle += Gdx.graphics.getDeltaTime() * 150f;
			} else {
				angle = 0f;
			}

			float imgW = img.getWidth();
			float imgH = img.getHeight();

			sb.setColor(color);
			sb.draw(img, pos.x - imgW/2, pos.y - imgH/2 + bobOffset,
					imgW/2, imgH/2, imgW, imgH, scale, scale,
					angle, 0, 0, (int)imgW, (int)imgH,
					false, false);
		}
	}

	public void renderTransMonster(SpriteBatch sb) {
		TextureAtlas atlas = ReflectionHacks.getPrivate(monster, AbstractCreature.class, "atlas");
		Skeleton skellie = ReflectionHacks.getPrivate(monster, AbstractCreature.class, "skeleton");
		Texture image = null;
		if (monster instanceof AbstractMonster) {
			image = AbstractMonsterHacks.getImg((AbstractMonster)monster);
		}
		Color tint = monster.tint.color;

		monster.state.update(Gdx.graphics.getDeltaTime());
		monster.state.apply(skellie);
		skellie.updateWorldTransform();
		skellie.setPosition(monster.drawX + monster.animX, monster.drawY + monster.animY);
		skellie.setColor(tint);
		skellie.setFlip(monster.flipHorizontal, monster.flipVertical);

		FrameBuffer fb = new FrameBuffer(Pixmap.Format.LuminanceAlpha, Settings.WIDTH, Settings.HEIGHT, false);

		// Non-SpriteBatch drawing cannot occur between begin and end.
		sb.end();

		fb.begin();
		CardCrawlGame.psb.begin();
		AbstractCreature.sr.draw(CardCrawlGame.psb, skellie);
		CardCrawlGame.psb.end();
		fb.end();
		Texture veryImportant = fb.getColorBufferTexture();

		// Begin the SpriteBatch again
		sb.begin();

		TextureRegion tr = new TextureRegion();
		tr.setRegion(veryImportant);

		sb.setColor(0, 0, 0, 1f);
		sb.draw(tr, 0, 0);

		sb.setBlendFunction(770, 771); // idk, is in AbstractMonster's func
	}

	@Override
	public float getHeight() {
		return 64f * Settings.scale;
	}

	@Override
	public float getWidth() {
		return 64f * Settings.scale;
	}

	@Override
	public float getMarginVert() {
		return getHeight();
	}

	@Override
	public float getMarginWhore() {
		return getWidth();
	}
}
