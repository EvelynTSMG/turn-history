package TurnHistory.hacks;

import basemod.ReflectionHacks;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.megacrit.cardcrawl.helpers.PowerTip;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.vfx.BobEffect;

public class AbstractMonsterHacks {
	public static PowerTip getIntentTip(AbstractMonster monster) {
		PowerTip tip = new PowerTip();
		PowerTip monsterTip = ReflectionHacks.getPrivate(monster, AbstractMonster.class, "intentTip");
		tip.imgRegion = monsterTip.imgRegion;
		tip.img = monsterTip.img;
		tip.header = monsterTip.header;
		tip.body = monsterTip.body;
		return tip;
	}

	public static BobEffect getBobEffect(AbstractMonster monster) {
		BobEffect bobEffect = new BobEffect();
		BobEffect monsterBobEffect = ReflectionHacks.getPrivate(monster, AbstractMonster.class, "bobEffect");
		bobEffect.y = monsterBobEffect.y;
		bobEffect.dist = monsterBobEffect.dist;
		bobEffect.speed = monsterBobEffect.speed;
		return bobEffect;
	}

	public static Texture getIntentImg(AbstractMonster monster) {
		return ReflectionHacks.privateMethod(AbstractMonster.class, "getIntentImg").invoke(monster);
	}

	public static Texture getIntentBg(AbstractMonster monster) {
		return ReflectionHacks.privateMethod(AbstractMonster.class, "getIntentBg").invoke(monster);
	}

	public static float getIntentAngle(AbstractMonster monster) {
		return ReflectionHacks.getPrivate(monster, AbstractMonster.class, "intentAngle");
	}

	public static int getIntentMultiAmt(AbstractMonster monster) {
		return ReflectionHacks.getPrivate(monster, AbstractMonster.class, "intentMultiAmt");
	}

	public static Color getIntentColor(AbstractMonster monster) {
		Color monsterColor = ReflectionHacks.getPrivate(monster, AbstractMonster.class, "intentColor");
		return new Color(monsterColor.r, monsterColor.g, monsterColor.b, monsterColor.a);
	}

	public static Texture getImg(AbstractMonster monster) {
		return ReflectionHacks.getPrivate(monster, AbstractMonster.class, "img");
	}
}

