package TurnHistory.hooks;

import TurnHistory.util.SubscriberManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.scenes.AbstractScene;
import javassist.CannotCompileException;
import javassist.CtBehavior;

import java.util.ArrayList;

@SpirePatch2(clz = AbstractDungeon.class, method="render")
public class PreCombatRoomForegroundRenderHook {
	private static class Locator extends SpireInsertLocator {
		public int[] Locate(CtBehavior ctMethodToPatch) throws CannotCompileException, PatchingException {
			Matcher finalMatcher = new Matcher.MethodCallMatcher(AbstractScene.class,
					"renderCombatRoomFg");
			return LineFinder.findInOrder(ctMethodToPatch, new ArrayList<>(), finalMatcher);
		}
	}

	@SpireInsertPatch(locator = Locator.class, localvars = {"sb"})
	public static void preCombatRoomForegroundRender(AbstractDungeon __instance, SpriteBatch sb) {
		SubscriberManager.publishPreCombatRoomForegroundRender(sb);
	}
}
