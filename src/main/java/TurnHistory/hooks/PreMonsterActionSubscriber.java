package TurnHistory.hooks;

import basemod.interfaces.ISubscriber;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public interface PreMonsterActionSubscriber extends ISubscriber {
	void receivePreMonsterAction(AbstractMonster monster);
}
