package TurnHistory.hooks;

import basemod.interfaces.ISubscriber;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface PreCombatRoomForegroundRenderSubscriber extends ISubscriber {
	void receivePreCombatRoomForegroundRender(SpriteBatch sb);
}
