package TurnHistory.hooks;

import TurnHistory.util.SubscriberManager;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.actions.GameActionManager;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import javassist.CannotCompileException;
import javassist.CtBehavior;

import java.util.ArrayList;

@SpirePatch2(clz = GameActionManager.class, method = "getNextAction")
public class PreMonsterActionHook {
	private static class Locator extends SpireInsertLocator {
		public int[] Locate(CtBehavior ctMethodToPatch) throws CannotCompileException, PatchingException {
			Matcher finalMatcher = new Matcher.MethodCallMatcher(AbstractMonster.class, "applyTurnPowers");
			return LineFinder.findInOrder(ctMethodToPatch, new ArrayList<>(), finalMatcher);
		}
	}

	@SpireInsertPatch(locator = Locator.class, localvars = {"m"})
	public static void postMonsterGetNextAction(GameActionManager __instance, AbstractMonster m) {
		SubscriberManager.publishPreMonsterAction(m);
	}
}

